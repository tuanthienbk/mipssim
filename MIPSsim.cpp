/*
 * On my honor, I have neither given nor received unauthorized aid on this assignment
 */

#include <string>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <list>

class Instruction
{
public:
	enum Type {
		ADD,
		ADDI,
		ADDIU,
		ADDU,
		AND,
		ANDI,
		BEQ,
		BNE,
		BGEZ,
		BGTZ,
		BLEZ,
		BLTZ,
		BREAK,
		J,
		LW,
		NOP,
		NOR,
		OR,
		ORI,
		SLL,
		SLT,
		SLTI,
		SLTU,
		SRA,
		SRL,
		SUB,
		SUBU,
		SW,
		XOR,
		MULT,
		DIV,
		MFHI,
		MFLO,
		UNKNOWN
	};

	std::string BINARY_STRING;
	Type TYPE;
	int IMMEDIATE;
    int SRC1;
	int SRC2;
	int DEST;
	int SA;
	int TARGET;

public:
	Instruction(std::string input) : BINARY_STRING(input)
	{
		TYPE = computeType();
		switch (TYPE) {
			case ADDI:
			case ANDI:
			case ORI:
			case SW:
			case LW:
			case ADDIU:
			case SLTI:
				IMMEDIATE = getImmediate1();
				break;
			case BEQ:
			case BGEZ:
			case BGTZ:
			case BNE:
			case BLEZ:
			case BLTZ:
				IMMEDIATE = getImmediate2();
				break;
			default :
				IMMEDIATE = 0;
				break;
		}
		char* end;
		DEST = std::strtol(BINARY_STRING.substr(6, 5).c_str(), &end, 2); // dest
		SRC1 = std::strtol(BINARY_STRING.substr(11, 5).c_str(), &end, 2); // src 1
		SRC2 = std::strtol(BINARY_STRING.substr(16, 5).c_str(), &end, 2); // src 2
		SA = std::strtol(BINARY_STRING.substr(21, 5).c_str(), &end, 2);

		if (TYPE == J)
			TARGET = getTarget();
		else
			TARGET = 0;

	}

	std::string toString() {

			static const std::string TypeString[] =  {
						"ADD",
						"ADDI",
						"ADDIU",
						"ADDU",
						"AND",
						"ANDI",
						"BEQ",
						"BNE",
						"BGEZ",
						"BGTZ",
						"BLEZ",
						"BLTZ",
						"BREAK",
						"J",
						"LW",
						"NOP",
						"NOR",
						"OR",
						"ORI",
						"SLL",
						"SLT",
						"SLTI",
						"SLTU",
						"SRA",
						"SRL",
						"SUB",
						"SUBU",
						"SW",
						"XOR",
						"MULT",
						"DIV",
						"MFHI",
						"MFLO"
					};
			std::ostringstream result;
			result << " [";
			switch (TYPE) {

			case ADDI:
			case ADDIU:
			case ANDI:
			case ORI:
			case SLTI:
				result << TypeString[TYPE] <<" R"  << DEST << ", R"  << SRC1 <<", #"  << IMMEDIATE;
				break;
			case SW:
			case LW:
				result << TypeString[TYPE] << " R"  << SRC1 << ", "  << IMMEDIATE << "(R"  << DEST <<")";
				break;

			case BEQ:
			case BNE:
				result << TypeString[TYPE] <<" R" << DEST << ", R" << SRC1 << ", #" << IMMEDIATE; break;
			case BREAK:
				result << "BREAK"; break;
			case J:
				result << TypeString[TYPE]<< " #" << TARGET; break;
			case BGEZ:
			case BGTZ:
			case BLEZ:
			case BLTZ:
				result << TypeString[TYPE] << " R" << DEST << ", #" << IMMEDIATE; break;
			case SLL:
			case SRL:
			case SRA:
				result << TypeString[TYPE] << " R" << DEST << ", R" << SRC1 << ", #" << SRC2; break;
			case SUB:
			case SUBU:
			case ADDU:
			case AND:
			case OR:
			case NOR:
			case XOR:
			case SLT:
			case SLTU:
			case ADD:
				result << TypeString[TYPE] << " R" << DEST << ", R" << SRC1 << ", R" << SRC2; break;
			case NOP:
				result << TypeString[TYPE]; break;
			case MULT:
			case DIV:
				result << TypeString[TYPE] << " R" << DEST << ", R" << SRC1; break;
			case MFHI:
			case MFLO:
				result << TypeString[TYPE] << " R" << DEST; break;
			default:
				result << "Instruction format not found.";
			}
			result << "]";
			return result.str();
		}
private:
	Type computeType() {

		std::string category = BINARY_STRING.substr(0, 3);
		std::string opcode = BINARY_STRING.substr(3, 3);

		if (BINARY_STRING.compare("00000000000000000000000000000000")==0) {
			return NOP;
		}
		if (category.compare("000")==0){
			if (opcode.compare("000")==0) return J;
			if (opcode.compare("001")==0) return BEQ;
			if (opcode.compare("010")==0) return BNE;
			if (opcode.compare("011")==0) return BGTZ;
			if (opcode.compare("100")==0) return SW;
			if (opcode.compare("101")==0) return LW;
			if (opcode.compare("110")==0) return BREAK;
		}
		if (category.compare("001")==0){
			if (opcode.compare("000")==0) return ADD;
			if (opcode.compare("001")==0) return SUB;
			if (opcode.compare("010")==0) return AND;
			if (opcode.compare("011")==0) return OR;
			if (opcode.compare("100")==0) return SRL;
			if (opcode.compare("101")==0) return SRA;

		}
		if (category.compare("010")==0){
			if (opcode.compare("000")==0) return ADDI;
			if (opcode.compare("001")==0) return ANDI;
			if (opcode.compare("010")==0) return ORI;

		}
		if (category.compare("011") == 0 && BINARY_STRING.substr(16,16).compare("0000000000000000")==0){
			if (opcode.compare("000")==0) return MULT;
			if (opcode.compare("001")==0) return DIV;

		}
		if (category.compare("100")==0 && BINARY_STRING.substr(11,21).compare("000000000000000000000")==0){
			if (opcode.compare("000")==0) return MFHI;
			if (opcode.compare("001")==0) return MFLO;

		}
		return UNKNOWN;
	}
	int getImmediate1()
	{
		std::string sign = BINARY_STRING.substr(16, 1);
		std::string imm = BINARY_STRING.substr(16,16);
		char *end;
		int immediate_pos = std::strtol(imm.c_str(), &end, 2);
		if (sign.compare("0")==0){
			return immediate_pos;
		}
		else{
			return (short)immediate_pos;
		}
	}
	int getImmediate2()
	{
		std::string sign = BINARY_STRING.substr(16, 1);
		std::string imm = BINARY_STRING.substr(16,16);

		char* end;
		imm += "00"; //shift left 2 bits
		if (sign.compare("0")==0){
			return (int)std::strtol(imm.c_str(), &end, 2);
		}
		else{
			imm = "11111111111111" + imm; //if negative sign extend to 32 bits
			return (int)std::strtoll(imm.c_str(), &end, 2);

		}
	}
	int getTarget()
	{
		std::string newTARGET = BINARY_STRING.substr(6, 26);
		newTARGET += "00"; //shift left 2 bits
		char* end;
//		std::cout << newTARGET <<" "<<std::strtoll(newTARGET.c_str(), &end, 2) << std::endl;
		return (int)std::strtol(newTARGET.c_str(), &end, 2);
	}
};
class MIPSsimulation
{
public:
	int base_addr; // base data address
	int base_ins_addr; // base instruction address
private:
	int R[34];

	std::vector<int> data;
	std::vector<Instruction> insList;
	int HI, LO;
public:
	MIPSsimulation()
	{
		std::fill(R, R+34, 0);

		base_addr = 0;
		base_ins_addr = 0;
		HI = 0;
		LO = 0;
	}
	void addInstruction(Instruction ins)
	{
		insList.push_back(ins);
	}
	void addData(int d)
	{
		data.push_back(d);
	}
	void print(std::ostream& stream)
	{
		int cycle = 1;
		int PC = 0;
		while ( PC < insList.size())
		{
			Instruction ins = insList[PC];

			stream << "--------------------\n";
			stream << "Cycle " << cycle <<":\t" << PC*4 + base_ins_addr << "\t" << ins.toString() <<"\n";
			stream <<"\n";
			cycle++;
			PC++;
			switch (ins.TYPE)
			{
			case Instruction::ADDI:
				R[ins.DEST] = R[ins.SRC1] + ins.IMMEDIATE; break;
			case Instruction::ADDIU:
				R[ins.DEST] = R[ins.SRC1] + ins.IMMEDIATE; break;
			case Instruction::SLTI:
				R[ins.DEST] = R[ins.SRC1]  < ins.IMMEDIATE; break;
			case Instruction::SW:
				setData(R[ins.SRC1], R[ins.DEST] + ins.IMMEDIATE); break;
			case Instruction::LW:
				R[ins.SRC1] = getData(R[ins.DEST] + ins.IMMEDIATE); break;
			case Instruction::BEQ:
				if (R[ins.DEST] == R[ins.SRC1]) PC = PC + (ins.IMMEDIATE)/4; break;
			case Instruction::BNE:
				if (R[ins.DEST] != R[ins.SRC1]) PC = PC + (ins.IMMEDIATE)/4; break;
			case Instruction::BREAK:
				break;
			case Instruction::J:
				PC = (ins.TARGET - base_ins_addr)/4; break;
			case Instruction::BGEZ:
				if (R[ins.DEST] >= 0) PC = PC + (ins.IMMEDIATE)/4; break;
			case Instruction::BGTZ:
				if (R[ins.DEST] > 0) PC = PC + (ins.IMMEDIATE)/4; break;
			case Instruction::BLEZ:
				if (R[ins.DEST] <= 0) PC = PC + (ins.IMMEDIATE)/4; break;
			case Instruction::BLTZ:
				if (R[ins.DEST] < 0) PC = PC + (ins.IMMEDIATE)/4; break;
			case Instruction::SLL:
				R[ins.DEST] = R[ins.SRC1] << R[ins.SRC2]; break;
			case Instruction::SRL:
			case Instruction::SRA:
				R[ins.DEST] = R[ins.SRC1] >> R[ins.SRC2]; break;
			case Instruction::SUB:
			case Instruction::SUBU:
				R[ins.DEST] = R[ins.SRC1] - R[ins.SRC2]; break;
			case Instruction::ADD:
			case Instruction::ADDU:
				R[ins.DEST] = R[ins.SRC1] + R[ins.SRC2]; break;
			case Instruction::AND:
				R[ins.DEST] = R[ins.SRC1] & R[ins.SRC2]; break;
			case Instruction::OR:
				R[ins.DEST] = R[ins.SRC1] | R[ins.SRC2]; break;
			case Instruction::NOR:
			case Instruction::XOR:
			case Instruction::SLT:
			case Instruction::SLTU:
			case Instruction::NOP:	 break;
			case Instruction::MULT:
			{
				long long int rs = (long long int)R[ins.DEST]*(long long int)R[ins.SRC1];
				HI = (int)(rs >> 32);
				LO = (int)(rs & ((1<<32)-1));
			}
				break;
			case Instruction::DIV:
			{
				HI = R[ins.DEST]%R[ins.SRC1];
				LO = R[ins.DEST]/R[ins.SRC1];
			}
				break;
			case Instruction::MFHI:
				R[ins.DEST] = HI; break;
			case Instruction::MFLO:
				R[ins.DEST] = LO; break;
			default: break;

			}

			stream <<"Registers\n";
			stream <<"R00:\t" << R[0] <<"\t" << R[1] <<"\t"<< R[2] <<"\t"<< R[3] <<"\t"<< R[4] <<"\t"<< R[5] <<"\t"<< R[6] <<"\t"<< R[7] <<"\n";
			stream <<"R08:\t" << R[8] <<"\t" << R[9] <<"\t"<< R[10] <<"\t"<< R[11] <<"\t"<< R[12] <<"\t"<< R[13] <<"\t"<< R[14] <<"\t"<< R[15] <<"\n";
			stream <<"R16:\t" << R[16] <<"\t" << R[17] <<"\t"<< R[18] <<"\t"<< R[19] <<"\t"<< R[20] <<"\t"<< R[21] <<"\t"<< R[22] <<"\t"<< R[23] <<"\n";
			stream <<"R24:\t" << R[24] <<"\t" << R[25] <<"\t"<< R[26] <<"\t"<< R[27] <<"\t"<< R[28] <<"\t"<< R[29] <<"\t"<< R[30] <<"\t"<< R[31] <<"\n";
			stream <<"\n";
			stream <<"Data\n";
			int i;
			for(i = 0; i < data.size()/8;i++)
				stream << base_addr + i*32 << ":\t" << data[8*i] <<"\t"<< data[8*i+1] <<"\t"<< data[8*i+2] <<"\t"<< data[8*i+3] <<"\t"<< data[8*i+4] <<"\t"<< data[8*i+5] <<"\t"<< data[8*i+6] <<"\t"<< data[8*i+7] <<"\n";
			if (i*8 < data.size())
			{
				stream << base_addr + i*32 << ":";
				for(int j = i*8; j < data.size(); j++)
					stream << "\t" << data[j];

			}
			stream <<"\n";

		}
	}
	void pipelineSim(std::ostream& stream)
	{
		int cycle = 1;
		int PC = 0;
		std::list<Instruction> buf1,buf2,buf3,buf4,buf5,buf6,buf8,buf11;
		bool buf1_empty,buf2_empty,buf3_empty,buf4_empty,buf5_empty,buf6_empty,buf8_empty,buf11_empty;

		std::list<std::pair<int,int> > WB;

		std::ostringstream waiting,executed,buf7,buf9,buf10,buf12;

		bool fetch_stall = false;

		bool R_busy[34];
		bool R_write_busy[34];
		bool write_busy[34];
		bool read_busy[34];

		std::fill(R_busy, R_busy+34, false);
		std::fill(R_write_busy, R_write_busy+34, false);

		int div_count = 0;
		int cond_counter = 0;

		bool done = false;

		while ( PC < insList.size())
		{
			std::list<std::pair<int,int> >::iterator itWB = WB.end(); itWB--;
			std::list<Instruction>::iterator itBuf1 = buf1.end(); itBuf1--;
			buf1_empty = buf1.empty();
			buf2_empty = buf2.empty();
			buf3_empty = buf3.empty();
			buf4_empty = buf4.empty();
			buf5_empty = buf5.empty();
			buf6_empty = buf6.empty();
			buf8_empty = buf8.empty();
			buf11_empty = buf11.empty();
			// Instruction fetch
			if (!fetch_stall)
			{
				for(int k = 0; k < 4; k++)
				{
					if (buf1.size() < 8)
					{
						buf1.push_back(insList[PC]);

						if (insList[PC].TYPE == Instruction::J || insList[PC].TYPE == Instruction::BEQ ||
								insList[PC].TYPE == Instruction::BNE || insList[PC].TYPE == Instruction::BGTZ
								|| insList[PC].TYPE == Instruction::BREAK)
						{
							waiting << insList[PC].toString();
							PC++;
							fetch_stall = true;
							break;
						}
						PC++;
					}
				}
			}
//
//			if (!buf1.empty() && buf1.front().TYPE == Instruction::BREAK && buf2_empty &&
//					buf3_empty && buf4_empty && buf5_empty && buf6_empty && buf8_empty &&
//					buf11_empty && WB.empty()) {
//				buf1.pop_front();
//				executed << "[BREAK]" ;
//				waiting.str("");
//			}

			std::fill(write_busy, write_busy+34, false);
			std::fill(read_busy, read_busy+34, false);

			for (std::list<Instruction>::iterator it=buf1.begin(); it != buf1.end();)
			{
				Instruction ins = *it;
				switch (ins.TYPE)
				{
				case Instruction::BEQ:
					if (!R_busy[ins.SRC1] && !R_busy[ins.DEST] &&
							!read_busy[ins.SRC1] && !read_busy[ins.DEST])
					{
						if (R[ins.DEST] == R[ins.SRC1]) PC = PC + (ins.IMMEDIATE)/4;
						it = buf1.erase(it);
						executed << ins.toString() ;
						waiting.str("");
						fetch_stall = false;

					} else {
						it++;
					}
					break;
				case Instruction::BNE:
					if (!R_busy[ins.SRC1] && !R_busy[ins.DEST] &&
							!read_busy[ins.SRC1] && !read_busy[ins.DEST])
					{
						if (R[ins.DEST] != R[ins.SRC1]) PC = PC + (ins.IMMEDIATE)/4;
						it = buf1.erase(it);
						executed << ins.toString() ;
						waiting.str("");
						fetch_stall = false;

					} else {
						it++;
					}
					break;

				case Instruction::J:
					PC = (ins.TARGET - base_ins_addr)/4;
					it = buf1.erase(it);
					executed << ins.toString() ;
					waiting.str("");
					fetch_stall = false;
					break;
				case Instruction::BGTZ:
					if (!R_busy[ins.DEST] && !read_busy[ins.DEST])
					{
						if (R[ins.DEST] > 0) PC = PC + (ins.IMMEDIATE)/4;
						it = buf1.erase(it);
						executed << ins.toString() ;
						waiting.str("");
						fetch_stall = false;
					} else {
						it++;
					}
					break;
				case Instruction::BREAK:

					it = buf1.erase(it);
					executed << " [BREAK]" ;
					waiting.str("");
					done = true;

					break;
				case Instruction::ADD:
				case Instruction::SUB:
				case Instruction::AND:
				case Instruction::OR:
				case Instruction::SRL:
				case Instruction::SRA:
					it++;
					write_busy[ins.SRC1] = true;
					write_busy[ins.SRC2] = true;
					write_busy[ins.DEST] = true;
					read_busy[ins.DEST] = true;
					break;

				case Instruction::ADDI:
				case Instruction::ANDI:
				case Instruction::ORI:
					it++;
					write_busy[ins.SRC1] = true;
					write_busy[ins.DEST] = true;
					read_busy[ins.DEST] = true;
					break;
				case Instruction::MFHI:
					it++;
					write_busy[32] = true;
					write_busy[ins.DEST] = true;
					read_busy[ins.DEST] = true;
					break;
				case Instruction::MFLO:
					it++;
					write_busy[33] = true;
					write_busy[ins.DEST] = true;
					read_busy[ins.DEST] = true;
					break;
				case Instruction::LW:
					it++;
					write_busy[ins.DEST] = true;
					write_busy[ins.SRC1] = true;
					read_busy[ins.SRC1] = true;
					break;
				case Instruction::SW:
					it++;
					write_busy[ins.DEST] = true;
					write_busy[ins.SRC1] = true;
					break;
				case Instruction::DIV:
					it++;
					write_busy[ins.DEST] = true;
					write_busy[ins.SRC1] = true;
					read_busy[32] = true;
					read_busy[33] = true;
					write_busy[32] = true;
					write_busy[33] = true;
					break;
				case Instruction::MULT:
					it++;
					write_busy[ins.DEST] = true;
					write_busy[ins.SRC1] = true;
					read_busy[33] = true;
					write_busy[33] = true;
					break;
				default: break;
				}
			}

			//Issue
			if (!buf1.empty()) {

				std::fill(write_busy, write_busy+34, false);
				std::fill(read_busy, read_busy+34, false);
				itBuf1++;
				for (std::list<Instruction>::iterator it=buf1.begin(); it != itBuf1;)
				{
					Instruction ins = *it;
					std::cout << ins << std::endl;
					switch (ins.TYPE)
					{
					case Instruction::BEQ:
						it++;
						break;
					case Instruction::BNE:
						it++;
						break;

					case Instruction::J:
						break;
					case Instruction::BGTZ:
						it++;
						break;
					case Instruction::BREAK:
						break;

					case Instruction::ADD:
					case Instruction::SUB:
					case Instruction::AND:
					case Instruction::OR:
					case Instruction::SRL:
					case Instruction::SRA:
						if (buf5.size() < 2 && !R_busy[ins.SRC1] && !R_busy[ins.SRC2] &&
								!read_busy[ins.SRC1] && !read_busy[ins.SRC2] &&
								!write_busy[ins.DEST] && !R_write_busy[ins.DEST]) {
							buf5.push_back(ins);
							it = buf1.erase(it);
							R_busy[ins.DEST] = true;
							R_write_busy[ins.DEST] = true;
//							R_write_busy[ins.SRC2] = true;
//							if (write_busy[ins.DEST]) R_write_busy[ins.DEST] = true;
						} else {
							it++;

						}
						write_busy[ins.SRC1] = true;
						write_busy[ins.SRC2] = true;
						write_busy[ins.DEST] = true;
						read_busy[ins.DEST] = true;
						break;

					case Instruction::ADDI:
					case Instruction::ANDI:
					case Instruction::ORI:
						if (buf5.size() < 2 && !R_busy[ins.SRC1] &&	!read_busy[ins.SRC1] &&
								!write_busy[ins.DEST] && !R_write_busy[ins.DEST]) {
							buf5.push_back(ins);
							it = buf1.erase(it);
							R_busy[ins.DEST] = true;
							R_write_busy[ins.DEST] = true;
//							if (write_busy[ins.DEST]) R_write_busy[ins.DEST] = true;
						} else {
							it++;

						}
						write_busy[ins.SRC1] = true;
						write_busy[ins.DEST] = true;
						read_busy[ins.DEST] = true;
						break;
					case Instruction::MFHI:
						if (buf5.size() < 2 && !R_busy[32] && !read_busy[32] &&
								!write_busy[ins.DEST] && !R_write_busy[ins.DEST]){
							buf5.push_back(ins);
							it = buf1.erase(it);
							R_busy[ins.DEST] = true;
							R_write_busy[ins.DEST] = true;
//							if (write_busy[ins.DEST]) R_write_busy[ins.DEST] = true;
						} else {
							it++;

						}
						write_busy[32] = true;
						write_busy[ins.DEST] = true;
						read_busy[ins.DEST] = true;
						break;
					case Instruction::MFLO:
						if (buf5.size() < 2 && !R_busy[33] && !read_busy[33]&&
								!write_busy[ins.DEST] && !R_write_busy[ins.DEST]){
							buf5.push_back(ins);
							it = buf1.erase(it);
							R_busy[ins.DEST] = true;
							R_write_busy[ins.DEST] = true;
//							if (write_busy[ins.DEST]) R_write_busy[ins.DEST] = true;
						} else {
							it++;

						}
						write_busy[33] = true;
						write_busy[ins.DEST] = true;
						read_busy[ins.DEST] = true;
						break;
					case Instruction::LW:
						if (buf2.size() < 2 && !R_busy[ins.DEST] && !read_busy[ins.DEST] &&
								!write_busy[ins.SRC1] && !R_write_busy[ins.SRC1]) {
							buf2.push_back(ins);
							it = buf1.erase(it);
							R_busy[ins.SRC1] = true;
							R_write_busy[ins.SRC1] = true;
//							if (write_busy[ins.SRC1]) R_write_busy[ins.SRC1] = true;
						} else {
							it++;

						}
						write_busy[ins.DEST] = true;
						write_busy[ins.SRC1] = true;
						read_busy[ins.SRC1] = true;
						break;
					case Instruction::SW:
						if (buf2.size() < 2 && !R_busy[ins.DEST] && !read_busy[ins.DEST] &&
								!R_busy[ins.SRC1] && !read_busy[ins.SRC1]) {
							buf2.push_back(ins);
							it = buf1.erase(it);
//							R_write_busy[ins.DEST] = true;
//							R_write_busy[ins.SRC1] = true;
						} else {
							it++;

						}
						write_busy[ins.DEST] = true;
						write_busy[ins.SRC1] = true;
						break;
					case Instruction::DIV:
						if (buf3.size() < 2 && !R_busy[ins.DEST] && !R_busy[ins.SRC1]  &&
								!read_busy[ins.DEST] && !read_busy[ins.SRC1]){

							buf3.push_back(ins);
							it = buf1.erase(it);
							R_busy[32] = true;
							R_busy[33] = true;
							R_write_busy[32] = true;
							R_write_busy[33] = true;
//							if (write_busy[32]) R_write_busy[32] = true;
//							if (write_busy[33]) R_write_busy[33] = true;
						} else {
							it++;

						}
						write_busy[ins.DEST] = true;
						write_busy[ins.SRC1] = true;
						read_busy[32] = true;
						read_busy[33] = true;
						write_busy[32] = true;
						write_busy[33] = true;
						break;
					case Instruction::MULT:
						if (buf4.size() < 2 && !R_busy[ins.DEST] && !R_busy[ins.SRC1] &&
								!read_busy[ins.DEST] && !read_busy[ins.SRC1] &&
								!write_busy[33] && !R_write_busy[33])
						{
							buf4.push_back(ins);
							it = buf1.erase(it);
							R_busy[33] = true;
							R_write_busy[33] = true;
//							R_write_busy[ins.SRC1] = true;
//							if (write_busy[33]) R_write_busy[33] = true;
						} else {
							it++;

						}
						write_busy[ins.DEST] = true;
						write_busy[ins.SRC1] = true;
						read_busy[33] = true;
						write_busy[33] = true;
						break;
					default: break;
					}
//					if (done) break;
				}

			}
//			if (done) break;
			//Execute

			if (cycle > cond_counter + 2)
			{
				if (!buf5_empty)
				{
					Instruction ins = buf5.front(); buf5.pop_front();
					int rs;
					switch (ins.TYPE)
					{
					case Instruction::ADDI:
						rs = R[ins.SRC1] + ins.IMMEDIATE;
//						R_write_busy[ins.SRC1] = false;
						break;
					case Instruction::ADD:
						rs = R[ins.SRC1] + R[ins.SRC2];
//						R_write_busy[ins.SRC1] = false;
//						R_write_busy[ins.SRC2] = false;
						break;
					case Instruction::SUB:
						rs = R[ins.SRC1] - R[ins.SRC2];
//						R_write_busy[ins.SRC1] = false;
//						R_write_busy[ins.SRC2] = false;
						break;
					case Instruction::AND:
						rs = R[ins.SRC1] & R[ins.SRC2];
//						R_write_busy[ins.SRC1] = false;
//						R_write_busy[ins.SRC2] = false;
						break;
					case Instruction::OR:
						rs = R[ins.SRC1] | R[ins.SRC2];
//						R_write_busy[ins.SRC1] = false;
//						R_write_busy[ins.SRC2] = false;
						break;
					case Instruction::SRL:
					case Instruction::SRA:
						rs = R[ins.SRC1] >> R[ins.SRC2];
//						R_write_busy[ins.SRC1] = false;
//						R_write_busy[ins.SRC2] = false;
						break;
					case Instruction::ANDI:
						rs = R[ins.SRC1] & ins.IMMEDIATE;
//						R_write_busy[ins.SRC1] = false;
						break;
					case Instruction::ORI:
						rs = R[ins.SRC1] | ins.IMMEDIATE;
//						R_write_busy[ins.SRC1] = false;
						break;
					case Instruction::MFHI:
						rs = HI;
//						R_write_busy[32] = false;
						break;
					case Instruction::MFLO:
						rs = LO;
//						R_write_busy[33] = false;
						break;

					default: break;
					}
					buf9 << " [" << rs << ", R" << ins.DEST << "]";
					WB.push_back(std::pair<int,int>(rs,ins.DEST));
				}
				if (!buf2_empty)
				{
					Instruction ins = buf2.front(); buf2.pop_front();
					switch (ins.TYPE)
					{
					case Instruction::SW:
//						R_write_busy[ins.DEST] = false;
//						R_write_busy[ins.SRC1] = false;
						break;
					case Instruction::LW:
//						R_write_busy[ins.DEST] = false;
						break;

					default: break;
					}
					buf6.push_back(ins);
				}
				if (!buf3_empty && div_count < 4)
				{
					if (div_count == 0) {
						Instruction ins = buf3.front();
//						R_write_busy[ins.DEST] = false;
//						R_write_busy[ins.SRC1] = false;
						HI = R[ins.DEST]%R[ins.SRC1];
						LO = R[ins.DEST]/R[ins.SRC1];
					}
					div_count++;
				}
				if (!buf3_empty && div_count == 4)
				{
					Instruction ins = buf3.front(); buf3.pop_front();
//					R_write_busy[ins.DEST] = false;
//					R_write_busy[ins.SRC1] = false;

					WB.push_back(std::pair<int,int>(HI,32));
					WB.push_back(std::pair<int,int>(LO,33));
					buf7 << " [<" << HI << ",HI>,<" << LO << ",LO>]";
					div_count = 0;
				}
				if (!buf4_empty)
				{
					Instruction ins = buf4.front(); buf4.pop_front();
//					R_write_busy[ins.DEST] = false;
//					R_write_busy[ins.SRC1] = false;
					long long int rs = (long long int)R[ins.DEST]*(long long int)R[ins.SRC1];
					HI = (int)(rs >> 32);
					LO = (int)(rs & ((1<<32)-1));
					buf8.push_back(ins);
				}
			}
			if (cycle > cond_counter + 3)
			{
				if (!buf6_empty)
				{
					Instruction ins = buf6.front();buf6.pop_front();

					switch (ins.TYPE)
					{
					case Instruction::SW:
						setData(R[ins.SRC1], R[ins.DEST] + ins.IMMEDIATE);
//						R_write_busy[ins.DEST] = false;
//						R_write_busy[ins.SRC1] = false;
						break;
					case Instruction::LW:
						int rs;
						rs = getData(R[ins.DEST] + ins.IMMEDIATE);
//						R_write_busy[ins.DEST] = false;
						WB.push_back(std::pair<int,int>(rs,ins.SRC1));
						buf10 << " [" << rs << ", R" << ins.SRC1 << "]";
						break;

					default: break;
					}

				}
				if (!buf8_empty)
				{
					Instruction ins = buf8.front();buf8.pop_front();
					buf11.push_back(ins);
				}

			}
			if (cycle > cond_counter + 4)
			{
				if (!buf11_empty)
				{
					buf11.pop_front();
					WB.push_back(std::pair<int,int>(LO,33));
					buf12 << " [" << LO << "]";
				}
			}

			itWB++;
			for (std::list<std::pair<int, int > >::iterator it=WB.begin(); it != itWB;)
//			while (!WB.empty())
			{
//				std::pair<int, int > p = WB.front();  WB.pop_front();
				std::pair<int, int > p = *it;
//				if (!R_write_busy[p.second])
//				{
//					R[p.second] = p.first;
//					R_busy[p.second] = false;
//					it = WB.erase(it);
//				} else {
//					it++;
//				}
				R[p.second] = p.first;
				R_busy[p.second] = false;
				R_write_busy[p.second] = false;
				it = WB.erase(it);
			}

			stream << "--------------------\n";
			stream << "Cycle " << cycle <<":"<<"\n";
			stream <<"\n";

			stream << "IF:\n";
			stream << "\tWaiting:" << waiting.str() <<"\n";
			stream << "\tExecuted:" << executed.str() <<"\n";
			executed.str("");
			stream << "Buf1:\n";
			int i = 0;
			for (std::list<Instruction>::iterator it=buf1.begin(); it != buf1.end(); ++it,++i)
				if ((*it).TYPE == Instruction::J || (*it).TYPE == Instruction::BEQ || (*it).TYPE == Instruction::BNE || (*it).TYPE == Instruction::BGTZ)
					stream <<"\tEntry " << i <<":\n";
				else
					stream <<"\tEntry " << i <<":" << (*it).toString() <<"\n";

 			for(i = buf1.size(); i < 8; i++)
				stream <<"\tEntry " << i <<":\n";

			stream << "Buf2:\n";
			i = 0;
			for (std::list<Instruction>::iterator it=buf2.begin(); it != buf2.end(); ++it,++i)
				stream <<"\tEntry " << i <<":" << (*it).toString() <<"\n";
			for(i = buf2.size(); i < 2; i++)
				stream <<"\tEntry " << i <<":\n";

			stream << "Buf3:\n";
			i = 0;
			for (std::list<Instruction>::iterator it=buf3.begin(); it != buf3.end(); ++it,++i)
				stream <<"\tEntry " << i <<":" << (*it).toString() <<"\n";
			for(i = buf3.size(); i < 2; i++)
				stream <<"\tEntry " << i <<":\n";

			stream << "Buf4:\n";
			i = 0;
			for (std::list<Instruction>::iterator it=buf4.begin(); it != buf4.end(); ++it,++i)
				stream <<"\tEntry " << i <<":" << (*it).toString() <<"\n";
			for(i = buf4.size(); i < 2; i++)
				stream <<"\tEntry " << i <<":\n";

			stream << "Buf5:\n";
			i = 0;
			for (std::list<Instruction>::iterator it=buf5.begin(); it != buf5.end(); ++it,++i)
				stream <<"\tEntry " << i <<":" << (*it).toString() <<"\n";
			for(i = buf5.size(); i < 2; i++)
				stream <<"\tEntry " << i <<":\n";

			stream << "Buf6:" << (buf6.empty() ? "":buf6.front().toString()) << "\n";
			stream << "Buf7:" << buf7.str() << "\n"; buf7.str("");
			stream << "Buf8:" << (buf8.empty() ? "":buf8.front().toString()) << "\n";
			stream << "Buf9:" << buf9.str() << "\n"; buf9.str("");
			stream << "Buf10:" << buf10.str() << "\n"; buf10.str("");
			stream << "Buf11:" << (buf11.empty() ? "":buf11.front().toString()) << "\n";
			stream << "Buf12:" << buf12.str() << "\n"; buf12.str("");
			stream << "\n";
			stream <<"Registers\n";
			stream <<"R00:\t" << R[0] <<"\t" << R[1] <<"\t"<< R[2] <<"\t"<< R[3] <<"\t"<< R[4] <<"\t"<< R[5] <<"\t"<< R[6] <<"\t"<< R[7] <<"\n";
			stream <<"R08:\t" << R[8] <<"\t" << R[9] <<"\t"<< R[10] <<"\t"<< R[11] <<"\t"<< R[12] <<"\t"<< R[13] <<"\t"<< R[14] <<"\t"<< R[15] <<"\n";
			stream <<"R16:\t" << R[16] <<"\t" << R[17] <<"\t"<< R[18] <<"\t"<< R[19] <<"\t"<< R[20] <<"\t"<< R[21] <<"\t"<< R[22] <<"\t"<< R[23] <<"\n";
			stream <<"R24:\t" << R[24] <<"\t" << R[25] <<"\t"<< R[26] <<"\t"<< R[27] <<"\t"<< R[28] <<"\t"<< R[29] <<"\t"<< R[30] <<"\t"<< R[31] <<"\n";
			stream <<"HI:\t" << R[32] <<"\n";
			stream <<"LO:\t" << R[33] <<"\n";
			stream <<"\n";
			stream <<"Data\n";

			for(i = 0; i < data.size()/8;i++)
				stream << base_addr + i*32 << ":\t" << data[8*i] <<"\t"<< data[8*i+1] <<"\t"<< data[8*i+2] <<"\t"<< data[8*i+3] <<"\t"<< data[8*i+4] <<"\t"<< data[8*i+5] <<"\t"<< data[8*i+6] <<"\t"<< data[8*i+7] <<"\n";
			if (i*8 < data.size())
			{
				stream << base_addr + i*32 << ":";
				for(int j = i*8; j < data.size(); j++)
					stream << "\t" << data[j];

			}
			stream <<"\n";
			if (done) break;
			cycle++;
		}

	}
private:
	int getData(int addr)
	{
		return data[(addr - base_addr)/4];
	}
	void setData(int d, int addr)
	{
		data[(addr - base_addr)/4] = d;
	}

};

int main(int argv, char* argc[])
{
	char* input_file = "";
	if (argv < 2)
		input_file = "sample.txt";
	else
		input_file = argc[1];
	std::ifstream fs(input_file, std::ios::in);

	if (fs.is_open())
	{
		std::ofstream ofs1("disassembly.txt", std::ios::out);

		std::string line = "";
		bool is_end = false;
		int base_addr = 256;
		MIPSsimulation sim;
		sim.base_ins_addr = base_addr;
		while(getline(fs, line))
		{
			if (!is_end){
				Instruction instruction(line);
//				std::cout << instruction.toString() << std::endl;
				ofs1 << line << "\t" << base_addr << "\t" << instruction.toString() << "\n";
				sim.addInstruction(instruction);
				is_end = instruction.TYPE == Instruction::BREAK;
				if (is_end) sim.base_addr = base_addr+4;
			} else {
				char* end;
				int d = (int)std::strtoll(line.c_str(), &end, 2);
				ofs1 << line << "\t" << base_addr << "\t" << d << "\n";
				sim.addData(d);
			}
			base_addr += 4;
		}

		fs.close();
		ofs1.close();

		std::ofstream ofs2("simulation.txt", std::ios::out);
//		sim.print(ofs2);
		sim.pipelineSim(ofs2);
		ofs2.close();

	} else {
		std::cerr << "File not found !" << std::endl;
	}



	return 0;
}
